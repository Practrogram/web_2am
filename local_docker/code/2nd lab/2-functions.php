<?php
echo "Функции";
echo "<br/><br/>";

echo "1. ";
function printStringReturnNumber(): int
{
    echo "fwewjfejwf ";
    $my_num = 1428;
    return $my_num;
}

echo printStringReturnNumber();

echo "<br/><br/>";
echo "2. ";

function increaseEnthusiasm(string $inlet): string
{
    return $inlet."!";
}

$myString = "I tend to communicate";
echo increaseEnthusiasm($myString);

echo "<br/>";
echo "3. ";

function repeatThreeTimes(string $spring): string
{
    return $spring.$spring.$spring;
}

$mantra = "enjoy ";
echo repeatThreeTimes($mantra);

echo "<br/>";
echo "4. ";

$ping = repeatThreeTimes("Move");
echo increaseEnthusiasm($ping);

echo "<br/><br/>";
echo "5. ";

function cut(string $dirk, int $howirst = 10): string
{
    $remainer = substr($dirk, 0, $howirst);
    return $remainer;
}

$example = "How everyone treats tourism, depends";
echo cut($example,20);

echo "<br/>";
echo "6. ";

function printFromTheArray(array $afdaf)
{
    var_dump(array_shift($afdaf));
    
    if (count($afdaf) !== 0){
        printFromTheArray($afdaf);
    }
}

$comfa = array(23, 423, 942, 81, 284, 812);
echo printFromTheArray($comfa);

echo "<br/>";
echo "7. ";

function CypherIndeed(int $num): int
{
    $plii = $num;
    $count = 0;
    
    while ($plii > 0){
        $plii = floor($plii/10);
        ++$count;
    }
    
    $diff = $count-1;
    $s = floor($num/pow(10,$diff));
    
    $arr = [];
    $arr[] = $s;
    while ($diff >= 1){
        $s = floor($num/pow(10,$diff-1))%10;
        $arr[] = $s;
        --$diff;
    }
    
    $result = array_sum($arr);
    if ($result > 9) { 
        $result = CypherIndeed($result); 
    }
    return $result;
}

$faler = 13579;
echo CypherIndeed($faler);
